'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">ecommerce documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/ACModule.html" data-type="entity-link" >ACModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-ACModule-7071514f85ea08bbe9137133b95b4a4ee14a5dde14555640264a6bdf1e2761374c1956c4115060074293d2a1244a6e35178e92d3d0ef3c9207881c650d54d619"' : 'data-target="#xs-controllers-links-module-ACModule-7071514f85ea08bbe9137133b95b4a4ee14a5dde14555640264a6bdf1e2761374c1956c4115060074293d2a1244a6e35178e92d3d0ef3c9207881c650d54d619"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-ACModule-7071514f85ea08bbe9137133b95b4a4ee14a5dde14555640264a6bdf1e2761374c1956c4115060074293d2a1244a6e35178e92d3d0ef3c9207881c650d54d619"' :
                                            'id="xs-controllers-links-module-ACModule-7071514f85ea08bbe9137133b95b4a4ee14a5dde14555640264a6bdf1e2761374c1956c4115060074293d2a1244a6e35178e92d3d0ef3c9207881c650d54d619"' }>
                                            <li class="link">
                                                <a href="controllers/ACController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ACController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-ACModule-7071514f85ea08bbe9137133b95b4a4ee14a5dde14555640264a6bdf1e2761374c1956c4115060074293d2a1244a6e35178e92d3d0ef3c9207881c650d54d619"' : 'data-target="#xs-injectables-links-module-ACModule-7071514f85ea08bbe9137133b95b4a4ee14a5dde14555640264a6bdf1e2761374c1956c4115060074293d2a1244a6e35178e92d3d0ef3c9207881c650d54d619"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-ACModule-7071514f85ea08bbe9137133b95b4a4ee14a5dde14555640264a6bdf1e2761374c1956c4115060074293d2a1244a6e35178e92d3d0ef3c9207881c650d54d619"' :
                                        'id="xs-injectables-links-module-ACModule-7071514f85ea08bbe9137133b95b4a4ee14a5dde14555640264a6bdf1e2761374c1956c4115060074293d2a1244a6e35178e92d3d0ef3c9207881c650d54d619"' }>
                                        <li class="link">
                                            <a href="injectables/ACService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ACService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AddressModule.html" data-type="entity-link" >AddressModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AddressModule-1f8a1626f72b622a4ee7961e0547259da31e7f06835b24fbef84293060a371150fda310288accc919a4d9d1e956410e436152931244023a6329be439b2b73dee"' : 'data-target="#xs-controllers-links-module-AddressModule-1f8a1626f72b622a4ee7961e0547259da31e7f06835b24fbef84293060a371150fda310288accc919a4d9d1e956410e436152931244023a6329be439b2b73dee"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AddressModule-1f8a1626f72b622a4ee7961e0547259da31e7f06835b24fbef84293060a371150fda310288accc919a4d9d1e956410e436152931244023a6329be439b2b73dee"' :
                                            'id="xs-controllers-links-module-AddressModule-1f8a1626f72b622a4ee7961e0547259da31e7f06835b24fbef84293060a371150fda310288accc919a4d9d1e956410e436152931244023a6329be439b2b73dee"' }>
                                            <li class="link">
                                                <a href="controllers/AddressController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AddressController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AddressModule-1f8a1626f72b622a4ee7961e0547259da31e7f06835b24fbef84293060a371150fda310288accc919a4d9d1e956410e436152931244023a6329be439b2b73dee"' : 'data-target="#xs-injectables-links-module-AddressModule-1f8a1626f72b622a4ee7961e0547259da31e7f06835b24fbef84293060a371150fda310288accc919a4d9d1e956410e436152931244023a6329be439b2b73dee"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AddressModule-1f8a1626f72b622a4ee7961e0547259da31e7f06835b24fbef84293060a371150fda310288accc919a4d9d1e956410e436152931244023a6329be439b2b73dee"' :
                                        'id="xs-injectables-links-module-AddressModule-1f8a1626f72b622a4ee7961e0547259da31e7f06835b24fbef84293060a371150fda310288accc919a4d9d1e956410e436152931244023a6329be439b2b73dee"' }>
                                        <li class="link">
                                            <a href="injectables/AddressService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AddressService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AppModule-1f028f6f0141adaec27c80a1d044ed827f97bffe1899caad3e2334bab8b1b7d32c018daca3622ab5500857736716c6277dafc43041ab248a3a3b7367d0b4d185"' : 'data-target="#xs-controllers-links-module-AppModule-1f028f6f0141adaec27c80a1d044ed827f97bffe1899caad3e2334bab8b1b7d32c018daca3622ab5500857736716c6277dafc43041ab248a3a3b7367d0b4d185"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AppModule-1f028f6f0141adaec27c80a1d044ed827f97bffe1899caad3e2334bab8b1b7d32c018daca3622ab5500857736716c6277dafc43041ab248a3a3b7367d0b4d185"' :
                                            'id="xs-controllers-links-module-AppModule-1f028f6f0141adaec27c80a1d044ed827f97bffe1899caad3e2334bab8b1b7d32c018daca3622ab5500857736716c6277dafc43041ab248a3a3b7367d0b4d185"' }>
                                            <li class="link">
                                                <a href="controllers/AppController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-1f028f6f0141adaec27c80a1d044ed827f97bffe1899caad3e2334bab8b1b7d32c018daca3622ab5500857736716c6277dafc43041ab248a3a3b7367d0b4d185"' : 'data-target="#xs-injectables-links-module-AppModule-1f028f6f0141adaec27c80a1d044ed827f97bffe1899caad3e2334bab8b1b7d32c018daca3622ab5500857736716c6277dafc43041ab248a3a3b7367d0b4d185"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-1f028f6f0141adaec27c80a1d044ed827f97bffe1899caad3e2334bab8b1b7d32c018daca3622ab5500857736716c6277dafc43041ab248a3a3b7367d0b4d185"' :
                                        'id="xs-injectables-links-module-AppModule-1f028f6f0141adaec27c80a1d044ed827f97bffe1899caad3e2334bab8b1b7d32c018daca3622ab5500857736716c6277dafc43041ab248a3a3b7367d0b4d185"' }>
                                        <li class="link">
                                            <a href="injectables/AppService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/CartModule.html" data-type="entity-link" >CartModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-CartModule-5cb9506e113d2613dc998f42cb99c871e18e69e067e279e5dedd5f5051a18abd07776a843ca3957531d4cf55addeec36eb2fd89890cf301d33212230ca5b5ccf"' : 'data-target="#xs-controllers-links-module-CartModule-5cb9506e113d2613dc998f42cb99c871e18e69e067e279e5dedd5f5051a18abd07776a843ca3957531d4cf55addeec36eb2fd89890cf301d33212230ca5b5ccf"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-CartModule-5cb9506e113d2613dc998f42cb99c871e18e69e067e279e5dedd5f5051a18abd07776a843ca3957531d4cf55addeec36eb2fd89890cf301d33212230ca5b5ccf"' :
                                            'id="xs-controllers-links-module-CartModule-5cb9506e113d2613dc998f42cb99c871e18e69e067e279e5dedd5f5051a18abd07776a843ca3957531d4cf55addeec36eb2fd89890cf301d33212230ca5b5ccf"' }>
                                            <li class="link">
                                                <a href="controllers/CartController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CartController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-CartModule-5cb9506e113d2613dc998f42cb99c871e18e69e067e279e5dedd5f5051a18abd07776a843ca3957531d4cf55addeec36eb2fd89890cf301d33212230ca5b5ccf"' : 'data-target="#xs-injectables-links-module-CartModule-5cb9506e113d2613dc998f42cb99c871e18e69e067e279e5dedd5f5051a18abd07776a843ca3957531d4cf55addeec36eb2fd89890cf301d33212230ca5b5ccf"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-CartModule-5cb9506e113d2613dc998f42cb99c871e18e69e067e279e5dedd5f5051a18abd07776a843ca3957531d4cf55addeec36eb2fd89890cf301d33212230ca5b5ccf"' :
                                        'id="xs-injectables-links-module-CartModule-5cb9506e113d2613dc998f42cb99c871e18e69e067e279e5dedd5f5051a18abd07776a843ca3957531d4cf55addeec36eb2fd89890cf301d33212230ca5b5ccf"' }>
                                        <li class="link">
                                            <a href="injectables/CartService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CartService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/RepositoryClass.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RepositoryClass</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/DepartmentsModule.html" data-type="entity-link" >DepartmentsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-DepartmentsModule-aabfe310a2cec2418dc5041f85b8e0410ca970d076a396cd83b9cdc0eb5e8c074f915cddf6064ef90ceabb2ceaf42f5b346837a7e0c1953be14904511e82e771"' : 'data-target="#xs-controllers-links-module-DepartmentsModule-aabfe310a2cec2418dc5041f85b8e0410ca970d076a396cd83b9cdc0eb5e8c074f915cddf6064ef90ceabb2ceaf42f5b346837a7e0c1953be14904511e82e771"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-DepartmentsModule-aabfe310a2cec2418dc5041f85b8e0410ca970d076a396cd83b9cdc0eb5e8c074f915cddf6064ef90ceabb2ceaf42f5b346837a7e0c1953be14904511e82e771"' :
                                            'id="xs-controllers-links-module-DepartmentsModule-aabfe310a2cec2418dc5041f85b8e0410ca970d076a396cd83b9cdc0eb5e8c074f915cddf6064ef90ceabb2ceaf42f5b346837a7e0c1953be14904511e82e771"' }>
                                            <li class="link">
                                                <a href="controllers/DepartmentsController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DepartmentsController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-DepartmentsModule-aabfe310a2cec2418dc5041f85b8e0410ca970d076a396cd83b9cdc0eb5e8c074f915cddf6064ef90ceabb2ceaf42f5b346837a7e0c1953be14904511e82e771"' : 'data-target="#xs-injectables-links-module-DepartmentsModule-aabfe310a2cec2418dc5041f85b8e0410ca970d076a396cd83b9cdc0eb5e8c074f915cddf6064ef90ceabb2ceaf42f5b346837a7e0c1953be14904511e82e771"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-DepartmentsModule-aabfe310a2cec2418dc5041f85b8e0410ca970d076a396cd83b9cdc0eb5e8c074f915cddf6064ef90ceabb2ceaf42f5b346837a7e0c1953be14904511e82e771"' :
                                        'id="xs-injectables-links-module-DepartmentsModule-aabfe310a2cec2418dc5041f85b8e0410ca970d076a396cd83b9cdc0eb5e8c074f915cddf6064ef90ceabb2ceaf42f5b346837a7e0c1953be14904511e82e771"' }>
                                        <li class="link">
                                            <a href="injectables/DepartmentsService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DepartmentsService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/MobileModule.html" data-type="entity-link" >MobileModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-MobileModule-fbf7181665a349152a12122b32991f0eeea760e6d8e41457e08effe529521b1c0e76bb4413d69dfcaf7e476f4acef185195a03041e2fd6c3863b559dbe004215"' : 'data-target="#xs-controllers-links-module-MobileModule-fbf7181665a349152a12122b32991f0eeea760e6d8e41457e08effe529521b1c0e76bb4413d69dfcaf7e476f4acef185195a03041e2fd6c3863b559dbe004215"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-MobileModule-fbf7181665a349152a12122b32991f0eeea760e6d8e41457e08effe529521b1c0e76bb4413d69dfcaf7e476f4acef185195a03041e2fd6c3863b559dbe004215"' :
                                            'id="xs-controllers-links-module-MobileModule-fbf7181665a349152a12122b32991f0eeea760e6d8e41457e08effe529521b1c0e76bb4413d69dfcaf7e476f4acef185195a03041e2fd6c3863b559dbe004215"' }>
                                            <li class="link">
                                                <a href="controllers/MobileController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MobileController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-MobileModule-fbf7181665a349152a12122b32991f0eeea760e6d8e41457e08effe529521b1c0e76bb4413d69dfcaf7e476f4acef185195a03041e2fd6c3863b559dbe004215"' : 'data-target="#xs-injectables-links-module-MobileModule-fbf7181665a349152a12122b32991f0eeea760e6d8e41457e08effe529521b1c0e76bb4413d69dfcaf7e476f4acef185195a03041e2fd6c3863b559dbe004215"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-MobileModule-fbf7181665a349152a12122b32991f0eeea760e6d8e41457e08effe529521b1c0e76bb4413d69dfcaf7e476f4acef185195a03041e2fd6c3863b559dbe004215"' :
                                        'id="xs-injectables-links-module-MobileModule-fbf7181665a349152a12122b32991f0eeea760e6d8e41457e08effe529521b1c0e76bb4413d69dfcaf7e476f4acef185195a03041e2fd6c3863b559dbe004215"' }>
                                        <li class="link">
                                            <a href="injectables/MobileService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MobileService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/PaymentModule.html" data-type="entity-link" >PaymentModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-PaymentModule-9033fc4948cbf296b1dde12244aad808f145067059e78d78f1f9eb9e569e3c675ec9cb0f7b2994a2695eced82e87b75e55acf043400bfeb27a90c5f2404f6afb"' : 'data-target="#xs-controllers-links-module-PaymentModule-9033fc4948cbf296b1dde12244aad808f145067059e78d78f1f9eb9e569e3c675ec9cb0f7b2994a2695eced82e87b75e55acf043400bfeb27a90c5f2404f6afb"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-PaymentModule-9033fc4948cbf296b1dde12244aad808f145067059e78d78f1f9eb9e569e3c675ec9cb0f7b2994a2695eced82e87b75e55acf043400bfeb27a90c5f2404f6afb"' :
                                            'id="xs-controllers-links-module-PaymentModule-9033fc4948cbf296b1dde12244aad808f145067059e78d78f1f9eb9e569e3c675ec9cb0f7b2994a2695eced82e87b75e55acf043400bfeb27a90c5f2404f6afb"' }>
                                            <li class="link">
                                                <a href="controllers/PaymentController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PaymentController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-PaymentModule-9033fc4948cbf296b1dde12244aad808f145067059e78d78f1f9eb9e569e3c675ec9cb0f7b2994a2695eced82e87b75e55acf043400bfeb27a90c5f2404f6afb"' : 'data-target="#xs-injectables-links-module-PaymentModule-9033fc4948cbf296b1dde12244aad808f145067059e78d78f1f9eb9e569e3c675ec9cb0f7b2994a2695eced82e87b75e55acf043400bfeb27a90c5f2404f6afb"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-PaymentModule-9033fc4948cbf296b1dde12244aad808f145067059e78d78f1f9eb9e569e3c675ec9cb0f7b2994a2695eced82e87b75e55acf043400bfeb27a90c5f2404f6afb"' :
                                        'id="xs-injectables-links-module-PaymentModule-9033fc4948cbf296b1dde12244aad808f145067059e78d78f1f9eb9e569e3c675ec9cb0f7b2994a2695eced82e87b75e55acf043400bfeb27a90c5f2404f6afb"' }>
                                        <li class="link">
                                            <a href="injectables/PaymentService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PaymentService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/RefrigeratorModule.html" data-type="entity-link" >RefrigeratorModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-RefrigeratorModule-e1c840960beafa48e5e5cb4e3e2abe2ff57546932c59ce7293f95a85ac81448ebb6180072186f1c52f954a9064478a956d4a0a2ac4da2efdcac4e31e15079918"' : 'data-target="#xs-controllers-links-module-RefrigeratorModule-e1c840960beafa48e5e5cb4e3e2abe2ff57546932c59ce7293f95a85ac81448ebb6180072186f1c52f954a9064478a956d4a0a2ac4da2efdcac4e31e15079918"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-RefrigeratorModule-e1c840960beafa48e5e5cb4e3e2abe2ff57546932c59ce7293f95a85ac81448ebb6180072186f1c52f954a9064478a956d4a0a2ac4da2efdcac4e31e15079918"' :
                                            'id="xs-controllers-links-module-RefrigeratorModule-e1c840960beafa48e5e5cb4e3e2abe2ff57546932c59ce7293f95a85ac81448ebb6180072186f1c52f954a9064478a956d4a0a2ac4da2efdcac4e31e15079918"' }>
                                            <li class="link">
                                                <a href="controllers/RefrigeratorController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RefrigeratorController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-RefrigeratorModule-e1c840960beafa48e5e5cb4e3e2abe2ff57546932c59ce7293f95a85ac81448ebb6180072186f1c52f954a9064478a956d4a0a2ac4da2efdcac4e31e15079918"' : 'data-target="#xs-injectables-links-module-RefrigeratorModule-e1c840960beafa48e5e5cb4e3e2abe2ff57546932c59ce7293f95a85ac81448ebb6180072186f1c52f954a9064478a956d4a0a2ac4da2efdcac4e31e15079918"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-RefrigeratorModule-e1c840960beafa48e5e5cb4e3e2abe2ff57546932c59ce7293f95a85ac81448ebb6180072186f1c52f954a9064478a956d4a0a2ac4da2efdcac4e31e15079918"' :
                                        'id="xs-injectables-links-module-RefrigeratorModule-e1c840960beafa48e5e5cb4e3e2abe2ff57546932c59ce7293f95a85ac81448ebb6180072186f1c52f954a9064478a956d4a0a2ac4da2efdcac4e31e15079918"' }>
                                        <li class="link">
                                            <a href="injectables/RefrigeratorService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RefrigeratorService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/RegisterModule.html" data-type="entity-link" >RegisterModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-RegisterModule-52bac8e826a12d3eda55d205032e6281c4cff1dd5fc2d9c5b0c5254ee1261e31d28193604cbbd7b2ede7017a10ce618ba7781910ce0368d6ff2b9eca7d406d4a"' : 'data-target="#xs-controllers-links-module-RegisterModule-52bac8e826a12d3eda55d205032e6281c4cff1dd5fc2d9c5b0c5254ee1261e31d28193604cbbd7b2ede7017a10ce618ba7781910ce0368d6ff2b9eca7d406d4a"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-RegisterModule-52bac8e826a12d3eda55d205032e6281c4cff1dd5fc2d9c5b0c5254ee1261e31d28193604cbbd7b2ede7017a10ce618ba7781910ce0368d6ff2b9eca7d406d4a"' :
                                            'id="xs-controllers-links-module-RegisterModule-52bac8e826a12d3eda55d205032e6281c4cff1dd5fc2d9c5b0c5254ee1261e31d28193604cbbd7b2ede7017a10ce618ba7781910ce0368d6ff2b9eca7d406d4a"' }>
                                            <li class="link">
                                                <a href="controllers/RegisterController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RegisterController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-RegisterModule-52bac8e826a12d3eda55d205032e6281c4cff1dd5fc2d9c5b0c5254ee1261e31d28193604cbbd7b2ede7017a10ce618ba7781910ce0368d6ff2b9eca7d406d4a"' : 'data-target="#xs-injectables-links-module-RegisterModule-52bac8e826a12d3eda55d205032e6281c4cff1dd5fc2d9c5b0c5254ee1261e31d28193604cbbd7b2ede7017a10ce618ba7781910ce0368d6ff2b9eca7d406d4a"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-RegisterModule-52bac8e826a12d3eda55d205032e6281c4cff1dd5fc2d9c5b0c5254ee1261e31d28193604cbbd7b2ede7017a10ce618ba7781910ce0368d6ff2b9eca7d406d4a"' :
                                        'id="xs-injectables-links-module-RegisterModule-52bac8e826a12d3eda55d205032e6281c4cff1dd5fc2d9c5b0c5254ee1261e31d28193604cbbd7b2ede7017a10ce618ba7781910ce0368d6ff2b9eca7d406d4a"' }>
                                        <li class="link">
                                            <a href="injectables/RegisterService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RegisterService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/TvModule.html" data-type="entity-link" >TvModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-TvModule-913ff5da5446c8f59dc2b9b4fd525cbec4eae1814a55346020bf5b83bff2faa88186c56a8a743479cb46737b46683781f186a610b4ed6bfd75e6a0aeb5144885"' : 'data-target="#xs-controllers-links-module-TvModule-913ff5da5446c8f59dc2b9b4fd525cbec4eae1814a55346020bf5b83bff2faa88186c56a8a743479cb46737b46683781f186a610b4ed6bfd75e6a0aeb5144885"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-TvModule-913ff5da5446c8f59dc2b9b4fd525cbec4eae1814a55346020bf5b83bff2faa88186c56a8a743479cb46737b46683781f186a610b4ed6bfd75e6a0aeb5144885"' :
                                            'id="xs-controllers-links-module-TvModule-913ff5da5446c8f59dc2b9b4fd525cbec4eae1814a55346020bf5b83bff2faa88186c56a8a743479cb46737b46683781f186a610b4ed6bfd75e6a0aeb5144885"' }>
                                            <li class="link">
                                                <a href="controllers/TvController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TvController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-TvModule-913ff5da5446c8f59dc2b9b4fd525cbec4eae1814a55346020bf5b83bff2faa88186c56a8a743479cb46737b46683781f186a610b4ed6bfd75e6a0aeb5144885"' : 'data-target="#xs-injectables-links-module-TvModule-913ff5da5446c8f59dc2b9b4fd525cbec4eae1814a55346020bf5b83bff2faa88186c56a8a743479cb46737b46683781f186a610b4ed6bfd75e6a0aeb5144885"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-TvModule-913ff5da5446c8f59dc2b9b4fd525cbec4eae1814a55346020bf5b83bff2faa88186c56a8a743479cb46737b46683781f186a610b4ed6bfd75e6a0aeb5144885"' :
                                        'id="xs-injectables-links-module-TvModule-913ff5da5446c8f59dc2b9b4fd525cbec4eae1814a55346020bf5b83bff2faa88186c56a8a743479cb46737b46683781f186a610b4ed6bfd75e6a0aeb5144885"' }>
                                        <li class="link">
                                            <a href="injectables/TvService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TvService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/WashingMachineModule.html" data-type="entity-link" >WashingMachineModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-WashingMachineModule-434bf08a4229c2572579af45af67d6aca5f4dc58045bf3af1bd9289e882e0496f28cb62381fcda0f8b661e9ac2e1e47ddb0e2e0c318bc26b9a3620222cf36bcc"' : 'data-target="#xs-controllers-links-module-WashingMachineModule-434bf08a4229c2572579af45af67d6aca5f4dc58045bf3af1bd9289e882e0496f28cb62381fcda0f8b661e9ac2e1e47ddb0e2e0c318bc26b9a3620222cf36bcc"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-WashingMachineModule-434bf08a4229c2572579af45af67d6aca5f4dc58045bf3af1bd9289e882e0496f28cb62381fcda0f8b661e9ac2e1e47ddb0e2e0c318bc26b9a3620222cf36bcc"' :
                                            'id="xs-controllers-links-module-WashingMachineModule-434bf08a4229c2572579af45af67d6aca5f4dc58045bf3af1bd9289e882e0496f28cb62381fcda0f8b661e9ac2e1e47ddb0e2e0c318bc26b9a3620222cf36bcc"' }>
                                            <li class="link">
                                                <a href="controllers/WashingMachineController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >WashingMachineController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-WashingMachineModule-434bf08a4229c2572579af45af67d6aca5f4dc58045bf3af1bd9289e882e0496f28cb62381fcda0f8b661e9ac2e1e47ddb0e2e0c318bc26b9a3620222cf36bcc"' : 'data-target="#xs-injectables-links-module-WashingMachineModule-434bf08a4229c2572579af45af67d6aca5f4dc58045bf3af1bd9289e882e0496f28cb62381fcda0f8b661e9ac2e1e47ddb0e2e0c318bc26b9a3620222cf36bcc"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-WashingMachineModule-434bf08a4229c2572579af45af67d6aca5f4dc58045bf3af1bd9289e882e0496f28cb62381fcda0f8b661e9ac2e1e47ddb0e2e0c318bc26b9a3620222cf36bcc"' :
                                        'id="xs-injectables-links-module-WashingMachineModule-434bf08a4229c2572579af45af67d6aca5f4dc58045bf3af1bd9289e882e0496f28cb62381fcda0f8b661e9ac2e1e47ddb0e2e0c318bc26b9a3620222cf36bcc"' }>
                                        <li class="link">
                                            <a href="injectables/WashingMachineService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >WashingMachineService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/WatchModule.html" data-type="entity-link" >WatchModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-WatchModule-3c94aa26b79b38c221388e941e579256167b9e88a4618bf5c7875938755baef24822f86a3eb30972f1a8a285349ff9adfbd6673e812ced320a120cb920df0242"' : 'data-target="#xs-controllers-links-module-WatchModule-3c94aa26b79b38c221388e941e579256167b9e88a4618bf5c7875938755baef24822f86a3eb30972f1a8a285349ff9adfbd6673e812ced320a120cb920df0242"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-WatchModule-3c94aa26b79b38c221388e941e579256167b9e88a4618bf5c7875938755baef24822f86a3eb30972f1a8a285349ff9adfbd6673e812ced320a120cb920df0242"' :
                                            'id="xs-controllers-links-module-WatchModule-3c94aa26b79b38c221388e941e579256167b9e88a4618bf5c7875938755baef24822f86a3eb30972f1a8a285349ff9adfbd6673e812ced320a120cb920df0242"' }>
                                            <li class="link">
                                                <a href="controllers/WatchController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >WatchController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-WatchModule-3c94aa26b79b38c221388e941e579256167b9e88a4618bf5c7875938755baef24822f86a3eb30972f1a8a285349ff9adfbd6673e812ced320a120cb920df0242"' : 'data-target="#xs-injectables-links-module-WatchModule-3c94aa26b79b38c221388e941e579256167b9e88a4618bf5c7875938755baef24822f86a3eb30972f1a8a285349ff9adfbd6673e812ced320a120cb920df0242"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-WatchModule-3c94aa26b79b38c221388e941e579256167b9e88a4618bf5c7875938755baef24822f86a3eb30972f1a8a285349ff9adfbd6673e812ced320a120cb920df0242"' :
                                        'id="xs-injectables-links-module-WatchModule-3c94aa26b79b38c221388e941e579256167b9e88a4618bf5c7875938755baef24822f86a3eb30972f1a8a285349ff9adfbd6673e812ced320a120cb920df0242"' }>
                                        <li class="link">
                                            <a href="injectables/WatchService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >WatchService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#controllers-links"' :
                                'data-target="#xs-controllers-links"' }>
                                <span class="icon ion-md-swap"></span>
                                <span>Controllers</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="controllers-links"' : 'id="xs-controllers-links"' }>
                                <li class="link">
                                    <a href="controllers/ACController.html" data-type="entity-link" >ACController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/AddressController.html" data-type="entity-link" >AddressController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/AppController.html" data-type="entity-link" >AppController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/CartController.html" data-type="entity-link" >CartController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/DepartmentsController.html" data-type="entity-link" >DepartmentsController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/MobileController.html" data-type="entity-link" >MobileController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/PaymentController.html" data-type="entity-link" >PaymentController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/RefrigeratorController.html" data-type="entity-link" >RefrigeratorController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/RegisterController.html" data-type="entity-link" >RegisterController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/TvController.html" data-type="entity-link" >TvController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/WashingMachineController.html" data-type="entity-link" >WashingMachineController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/WatchController.html" data-type="entity-link" >WatchController</a>
                                </li>
                            </ul>
                        </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#entities-links"' :
                                'data-target="#xs-entities-links"' }>
                                <span class="icon ion-ios-apps"></span>
                                <span>Entities</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="entities-links"' : 'id="xs-entities-links"' }>
                                <li class="link">
                                    <a href="entities/Address.html" data-type="entity-link" >Address</a>
                                </li>
                                <li class="link">
                                    <a href="entities/AirConditioner.html" data-type="entity-link" >AirConditioner</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Cart.html" data-type="entity-link" >Cart</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Departments.html" data-type="entity-link" >Departments</a>
                                </li>
                                <li class="link">
                                    <a href="entities/HomeSpecifications.html" data-type="entity-link" >HomeSpecifications</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Mobile.html" data-type="entity-link" >Mobile</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Payment.html" data-type="entity-link" >Payment</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Refrigerator.html" data-type="entity-link" >Refrigerator</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Register.html" data-type="entity-link" >Register</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Specification.html" data-type="entity-link" >Specification</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Tv.html" data-type="entity-link" >Tv</a>
                                </li>
                                <li class="link">
                                    <a href="entities/UserLogin.html" data-type="entity-link" >UserLogin</a>
                                </li>
                                <li class="link">
                                    <a href="entities/WashingMachine.html" data-type="entity-link" >WashingMachine</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Watches.html" data-type="entity-link" >Watches</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/customException.html" data-type="entity-link" >customException</a>
                            </li>
                            <li class="link">
                                <a href="classes/ValidationExceptionFilter.html" data-type="entity-link" >ValidationExceptionFilter</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/ACService.html" data-type="entity-link" >ACService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AddressService.html" data-type="entity-link" >AddressService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AppService.html" data-type="entity-link" >AppService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CartService.html" data-type="entity-link" >CartService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/DepartmentsService.html" data-type="entity-link" >DepartmentsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LoggerMiddleware.html" data-type="entity-link" >LoggerMiddleware</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MobileService.html" data-type="entity-link" >MobileService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PaymentService.html" data-type="entity-link" >PaymentService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RefrigeratorService.html" data-type="entity-link" >RefrigeratorService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RegisterService.html" data-type="entity-link" >RegisterService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RepositoryClass.html" data-type="entity-link" >RepositoryClass</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TvService.html" data-type="entity-link" >TvService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ValidationPipes.html" data-type="entity-link" >ValidationPipes</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/WashingMachineService.html" data-type="entity-link" >WashingMachineService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/WatchService.html" data-type="entity-link" >WatchService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/SwaggerConfig.html" data-type="entity-link" >SwaggerConfig</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});