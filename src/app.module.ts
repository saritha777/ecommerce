import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AddressModule } from './address/address.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CartModule } from './cart/cart.module';
import { DepartmentsModule } from './department/department.module';
import { MobileModule } from './department/electronics/mobiles/mobiles.module';
import { TvModule } from './department/electronics/tv/tv.module';
import { WatchModule } from './department/electronics/watches/watches.module';
import { ACModule } from './department/homeappliances/ac/ac.module';
import { RefrigeratorModule } from './department/homeappliances/refrigerator/refrigerator.module';
import { WashingMachineModule } from './department/homeappliances/washingmachine/washingmachine.module';
import { LoggerMiddleware } from './logger/logger.middleware';
import { PaymentModule } from './payment/payment.module';
import { RegisterModule } from './register/register.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: 'root',
      database: 'ecommerce',
      autoLoadEntities: true,
      synchronize: true
    }),
    RegisterModule,
    AddressModule,
    DepartmentsModule,
    MobileModule,
    TvModule,
    WatchModule,
    RefrigeratorModule,
    ACModule,
    WashingMachineModule,
    CartModule,
    PaymentModule
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes('*');
  }
}
