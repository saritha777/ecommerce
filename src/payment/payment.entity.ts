import { ApiProperty } from '@nestjs/swagger';
import { UserLogin } from '../register/entity/login.entity';
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { IsInt, IsString } from 'class-validator';

@Entity()
export class Payment {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsInt()
  @Column()
  cartId: number;

  @ApiProperty()
  @IsInt()
  @Column()
  userId: number;

  @Column()
  userName: string;

  @ApiProperty()
  @IsInt()
  @Column()
  cardNumber: number;

  @ApiProperty()
  @IsInt()
  @Column()
  cvv: number;

  @ApiProperty()
  @IsString()
  @Column()
  expiry: string;

  @ApiProperty()
  @IsString()
  @Column()
  cardHolderName: string;

  @ApiProperty()
  @IsInt()
  @Column()
  addressId: number;

  @OneToOne(() => UserLogin, (login) => login.payment, {
    cascade: true
  })
  login: UserLogin;
}
