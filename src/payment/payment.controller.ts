import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { request } from 'http';
import { Payment } from './payment.entity';
import { PaymentService } from './payment.service';
import { Request } from 'express';

@ApiTags('Payment')
@Controller('Payment')
export class PaymentController {
  constructor(private readonly paymentService: PaymentService) {}

  @Post('/payment')
  addPayment(
    @Body() payment: Payment,
    @Req() request: Request
  ): Promise<Payment> {
    return this.paymentService.addPayment(payment, request);
  }

  @Get('/payment')
  getAllPayment(): Promise<Payment[]> {
    return this.paymentService.getAllPayment();
  }

  @Get('payment/:id')
  getPaymentDataById(@Param('id') id: number): Promise<Payment> {
    return this.paymentService.getPaymentDataById(id);
  }

  /*  @Put('/payment/:id')
  updatePayment(@Param('id') id: number, @Body() payment: Payment) {
    return this.paymentService.updatePayment(id, payment);
  } */

  @Delete('payment/:id')
  deleteAddress(@Param('id') id: number) {
    return this.paymentService.cancelPayment(id);
  }
}
