import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserLogin } from '../register/entity/login.entity';
import { Address } from '../address/address.entity';
import { PaymentController } from './payment.controller';
import { Payment } from './payment.entity';
import { PaymentService } from './payment.service';
import { Cart } from '../cart/cart.entity';
import { JwtModule } from '@nestjs/jwt';

@Module({
  imports: [
    TypeOrmModule.forFeature([Payment, UserLogin, Address, Cart]),
    JwtModule.register({
      secret: 'secret',
      signOptions: { expiresIn: '30m' }
    })
  ],
  controllers: [PaymentController],
  providers: [PaymentService]
})
export class PaymentModule {}
