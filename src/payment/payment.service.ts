import {
  HttpCode,
  HttpException,
  HttpStatus,
  Injectable,
  Logger,
  UnauthorizedException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Address } from '../address/address.entity';
import { Repository } from 'typeorm';
import { Payment } from './payment.entity';
import { UserLogin } from '../register/entity/login.entity';
import { Cart } from '../cart/cart.entity';
import { Request, Response } from 'express';
import { JwtService } from '@nestjs/jwt';
import { request } from 'http';
import { customException } from '../customexception/custom.exception';

@Injectable()
export class PaymentService {
  logger: Logger;
  constructor(
    @InjectRepository(Payment)
    private paymentRepository: Repository<Payment>,
    @InjectRepository(UserLogin)
    private loginRepository: Repository<UserLogin>,
    @InjectRepository(Address)
    private addressRepository: Repository<Address>,
    @InjectRepository(Cart)
    private cartRepository: Repository<Cart>,
    private jwtService: JwtService
  ) {
    this.logger = new Logger(PaymentService.name);
  }

  async addPayment(payment: Payment, request: Request) {
    if (this.findUser(request)) {
      const paymentdata = new Payment();

      const logindata: UserLogin = await this.loginRepository.findOne({
        id: payment.userId
      });

      const addressResult: Address = await this.addressRepository.findOne({
        addressId: payment.addressId
      });
      const cartResult: Cart = await this.cartRepository.findOne({
        id: payment.cartId
      });

      paymentdata.cartId = cartResult.id;
      paymentdata.addressId = addressResult.addressId;
      paymentdata.userId = logindata.id;
      paymentdata.userName = logindata.name;
      paymentdata.cardHolderName = payment.cardHolderName;
      paymentdata.cardNumber = payment.cardNumber;
      paymentdata.cvv = payment.cvv;
      paymentdata.expiry = payment.expiry;
      this.logger.log('payment done successfully');
      const result = await this.paymentRepository.save(paymentdata);
      // await this.cartRepository.delete({userName:payment.userName});
      return result;
    } else {
      throw new HttpException(
        'user not logedin...... please login',
        HttpStatus.NOT_FOUND
      );
    }
  }
  async findUser(request: Request) {
    try {
      const cookie = request.cookies['jwt'];

      const data = await this.jwtService.verifyAsync(cookie);
      if (!data) {
        throw new UnauthorizedException();
      }
      const user = await this.loginRepository.findOne({ id: data.id });
      const { password, ...result } = user;
      return result;
    } catch (e) {
      throw new UnauthorizedException();
    }
  }

  async getAllPayment(): Promise<Payment[]> {
    this.logger.log('getting all payments');
    return await this.paymentRepository
      .find()
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'no data in the list',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException('no data in the list', HttpStatus.NOT_FOUND);
      });
  }

  async getPaymentDataById(id: number): Promise<Payment> {
    this.logger.log('getting payments by id');
    return await this.paymentRepository
      .findOne({ id })
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'payment not done in this id',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException(
          'payment not done in this id',
          HttpStatus.NOT_FOUND
        );
      });
  }

  /*  async updatePayment(id: number, payment: Payment) {
        const paymentdata = new Payment();
       
        const logindata: UserLogin= await this.loginRepository.findOne({
          id: payment.userId, 
        });
   
        const addressResult: Address = await this.addressRepository.findOne({
          addressId: payment.addressId,
        });
        const cartResult: Cart = await this.cartRepository.findOne({
            id: payment.cartId,     
          });
   
        paymentdata.cartId = cartResult.id;
        paymentdata.addressId = addressResult.addressId;
        paymentdata.userId = logindata.id;
        paymentdata.userName = logindata.name;
        paymentdata.cardHolderName = payment.cardHolderName;
        paymentdata.cardNumber = payment.cardNumber;
        paymentdata.cvv = payment.cvv;
        paymentdata.expiry = payment.expiry;
      
        this.paymentRepository.update({ id: id }, payment);
        return 'successfully updated';
    } */

  async cancelPayment(addressId: number) {
    this.logger.log('canceled payment');
    return await this.paymentRepository.delete({ addressId });
  }
}
