import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Mobile } from '../department/electronics/mobiles/mobile.entity';
import { Tv } from '../department/electronics/tv/television.entity';
import { Watches } from '../department/electronics/watches/watch.entity';
import { AirConditioner } from '../department/homeappliances/ac/AC.entity';
import { Refrigerator } from '../department/homeappliances/refrigerator/Refrigerator.entity';
import { WashingMachine } from '../department/homeappliances/washingmachine/WashingMachine.entity';
import { UserLogin } from '../register/entity/login.entity';
import { Register } from '../register/entity/register.entity';
import { RepositoryClass } from '../repository/repository';
import { CartController } from './cart.controller';
import { Cart } from './cart.entity';
import { CartService } from './cart.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Cart,
      Mobile,
      Tv,
      AirConditioner,
      WashingMachine,
      Refrigerator,
      Watches,
      UserLogin,
      Register
    ]),
    JwtModule.register({
      secret: 'secret',
      signOptions: { expiresIn: '30m' }
    })
  ],
  controllers: [CartController],
  providers: [CartService, RepositoryClass]
})
export class CartModule {}
