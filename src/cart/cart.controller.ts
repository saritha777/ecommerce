import { Body, Controller, Delete, Param, Post, Req } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Cart } from './cart.entity';
import { CartService } from './cart.service';
import { Request } from 'express';

@ApiTags('Cart')
@Controller('/Cart')
export class CartController {
  constructor(private readonly cartService: CartService) {}

  @Post('/cart')
  addCart(@Body() cart: Cart, @Req() request: Request) {
    return this.cartService.addItemsIntoCart(cart, request);
  }

  @Delete('/cart/:userName')
  delete(@Param('userName') userName: string) {
    return this.cartService.delete(userName);
  }
}
