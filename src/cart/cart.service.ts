import {
  HttpException,
  HttpStatus,
  Injectable,
  Logger,
  UnauthorizedException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserLogin } from '../register/entity/login.entity';
import { getManager, Repository } from 'typeorm';
import { Cart } from './cart.entity';
import { RepositoryClass } from '../repository/repository';
import { JwtService } from '@nestjs/jwt';
import { Request } from 'express';

@Injectable()
export class CartService {
  logger: Logger;
  constructor(
    @InjectRepository(Cart)
    private cartRepository: Repository<Cart>,
    private readonly repositoryClass: RepositoryClass,
    private jwtService: JwtService
  ) {
    this.logger = new Logger(CartService.name);
  }

  async addItemsIntoCart(data: Cart, request: Request) {
    if (this.findUser(request)) {
      const cartdata = new Cart();
      const logindata: UserLogin =
        await this.repositoryClass.loginRepository.findOne({
          name: data.userName
        });
      cartdata.userName = logindata.name;

      const mobiledata = await this.repositoryClass.mobileRepository.findOne({
        brandName: data.productName
      });
      const tvdata = await this.repositoryClass.tvRepository.findOne({
        brandName: data.productName
      });
      const watchdata = await this.repositoryClass.watchRepository.findOne({
        brandName: data.productName
      });

      const acdata = await this.repositoryClass.acRepository.findOne({
        brand: data.productName
      });
      const refrigeratordata =
        await this.repositoryClass.refrigeratorRepository.findOne({
          brand: data.productName
        });
      const washingMachinedata =
        await this.repositoryClass.washingMachineRepository.findOne({
          brand: data.productName
        });
      const Catagery: string = data.category;
      cartdata.userName = logindata.name;
      cartdata.quantity = data.quantity;
      cartdata.category = data.category;
      switch (Catagery) {
        case 'mobile':
          if (!mobiledata) {
            throw new HttpException(
              'mobile is not available plaese check....',
              HttpStatus.BAD_REQUEST
            );
          }
          cartdata.productName = mobiledata.brandName;
          cartdata.price = mobiledata.price;
          cartdata.totalPrice = cartdata.price * cartdata.quantity;
          break;
        case 'tv':
          if (!tvdata) {
            throw new HttpException(
              'tv is not available plaese check....',
              HttpStatus.BAD_REQUEST
            );
          }
          cartdata.productName = tvdata.brandName;
          cartdata.price = tvdata.price;
          cartdata.totalPrice = cartdata.price * cartdata.quantity;
          break;
        case 'watches':
          if (!watchdata) {
            throw new HttpException(
              'watch is not available plaese check....',
              HttpStatus.BAD_REQUEST
            );
          }
          cartdata.productName = watchdata.brandName;
          cartdata.price = watchdata.price;
          cartdata.totalPrice = cartdata.price * cartdata.quantity;
          break;
        case 'ac':
          if (!acdata) {
            throw new HttpException(
              'ac is not available plaese check....',
              HttpStatus.BAD_REQUEST
            );
          }
          cartdata.productName = acdata.brand;
          cartdata.price = acdata.price;
          cartdata.totalPrice = cartdata.price * cartdata.quantity;
          break;
        case 'refrigerator':
          if (!refrigeratordata) {
            throw new HttpException(
              'refrigerator is not available plaese check....',
              HttpStatus.BAD_REQUEST
            );
          }
          cartdata.productName = refrigeratordata.brand;
          cartdata.price = refrigeratordata.price;
          cartdata.totalPrice = cartdata.price * cartdata.quantity;
          break;
        case 'washingMachine':
          if (!washingMachinedata) {
            throw new HttpException(
              'washingMachine is not available plaese check....',
              HttpStatus.BAD_REQUEST
            );
          }
          cartdata.productName = washingMachinedata.brand;
          cartdata.price = washingMachinedata.price;
          cartdata.totalPrice = cartdata.price * cartdata.quantity;
          break;
      }
      this.logger.log('added items into the cart successfully');
      await this.cartRepository.save(cartdata);

      const cartItems = await this.cartRepository.find();
      // const cartItems = await getManager().query(`select * from cart where useName = "${cartdata.userName}"`)
      // const name = await this.cartRepository.findOne({userName:data.userName});
      const amount = await getManager().query(
        `SELECT SUM(totalPrice) "price" FROM cart where userName = "${cartdata.userName}";`
      );
      const product = amount[0];
      const result = {
        Allitems: cartItems,
        priceValue: product.price
      };
      return result;
    } else {
      throw new HttpException(
        'user not logedin...... please login',
        HttpStatus.NOT_FOUND
      );
    }
  }

  async findUser(request: Request) {
    try {
      const cookie = request.cookies['jwt'];

      const data = await this.jwtService.verifyAsync(cookie);
      if (!data) {
        throw new UnauthorizedException();
      }
      const user = await this.repositoryClass.loginRepository.findOne({
        id: data.id
      });
      const { password, ...result } = user;
      return result;
    } catch (e) {
      throw new UnauthorizedException();
    }
  }

  async delete(userName: string) {
    //  const data = await this.cartRepository.find({userName: userName})
    //  console.log(data);
    this.logger.log('deleted cart items successfully');
    return await this.cartRepository.delete({ userName: userName });
  }
}
