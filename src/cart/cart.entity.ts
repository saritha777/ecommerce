import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Cart {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column()
  userName: string;

  @ApiProperty()
  @IsString()
  @Column()
  category: string;

  @ApiProperty()
  @IsString()
  @Column()
  productName: string;

  @ApiProperty()
  @IsInt()
  @Column()
  quantity: number;

  @Column()
  price: number;

  @Column()
  totalPrice: number;
}
