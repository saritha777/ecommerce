import { InjectRepository } from '@nestjs/typeorm';
import { Mobile } from '../department/electronics/mobiles/mobile.entity';
import { Tv } from '../department/electronics/tv/television.entity';
import { Repository } from 'typeorm';
import { UserLogin } from '../register/entity/login.entity';
import { Injectable } from '@nestjs/common';
import { Watches } from '../department/electronics/watches/watch.entity';
import { AirConditioner } from '../department/homeappliances/ac/AC.entity';

import { WashingMachine } from '../department/homeappliances/washingmachine/WashingMachine.entity';
import { Register } from '../register/entity/register.entity';
import { Refrigerator } from '../department/homeappliances/refrigerator/Refrigerator.entity';

@Injectable()
export class RepositoryClass {
  @InjectRepository(Register)
  public registerRepository: Repository<Register>;

  @InjectRepository(UserLogin)
  public loginRepository: Repository<UserLogin>;

  @InjectRepository(Mobile)
  public mobileRepository: Repository<Mobile>;

  @InjectRepository(Tv)
  public tvRepository: Repository<Tv>;

  @InjectRepository(Watches)
  public watchRepository: Repository<Watches>;

  @InjectRepository(AirConditioner)
  public acRepository: Repository<AirConditioner>;

  @InjectRepository(WashingMachine)
  public washingMachineRepository: Repository<WashingMachine>;

  @InjectRepository(Refrigerator)
  public refrigeratorRepository: Repository<Refrigerator>;
}
