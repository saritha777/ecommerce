import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req,
  Res
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { UserLogin } from './entity/login.entity';
import { Register } from './entity/register.entity';
import { RegisterService } from './register.service';
import { Request, Response } from 'express';

@ApiTags('Register')
@Controller('/register')
export class RegisterController {
  constructor(private readonly registerService: RegisterService) {}

  @Post()
  addCustomer(@Body() data: Register): Promise<Register> {
    return this.registerService.customerRegister(data);
  }

  // checking user is logined or not
  @Post('/login')
  async login(
    @Body() login: UserLogin,
    @Res({ passthrough: true }) response: Response
  ) {
    return this.registerService.loginUser(login, response);
  }

  //find user using cookie
  @Get('/userlogin')
  async user(@Req() request: Request) {
    return this.registerService.findUser(request);
  }

  // logout user
  @Post('/logout')
  async logout(@Res({ passthrough: true }) response: Response) {
    return this.registerService.logout(response);
  }

  @Get()
  getCustomer(): Promise<Register[]> {
    return this.registerService.getCustomer();
  }

  @Get('/:firstName')
  getCustomerByName(@Param('firstName') firstName: string): Promise<Register> {
    return this.registerService.getCustomerByName(firstName);
  }

  @Put('/:id')
  updateCustomer(@Param('id') id: number, @Body() data: Register): string {
    return this.registerService.updateCustomer(id, data);
  }

  @Delete('/:id')
  deleteCustomer(@Param('id') id: number) {
    return this.registerService.deleteCustomer(id);
  }
}
