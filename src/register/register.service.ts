import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Injectable,
  Logger,
  NotFoundException,
  Res,
  UnauthorizedException
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserLogin } from './entity/login.entity';
import { Register } from './entity/register.entity';
import { Request, Response } from 'express';
import * as bcrypt from 'bcryptjs';
import { customException } from '../customexception/custom.exception';

@Injectable()
export class RegisterService {
  logger: Logger;
  constructor(
    @InjectRepository(Register)
    private registerRepository: Repository<Register>,
    @InjectRepository(UserLogin)
    private loginRepository: Repository<UserLogin>,
    private jwtService: JwtService
  ) {
    this.logger = new Logger(RegisterService.name);
  }

  async customerRegister(data: Register): Promise<Register> {
    const pass = await bcrypt.hash(data.password, 10);
    const customer = new Register();
    const login = new UserLogin();
    customer.firstName = login.name = data.firstName;
    customer.password = login.password = pass;
    customer.lastName = data.lastName;
    customer.email = data.email;
    customer.phoneNo = data.phoneNo;
    customer.address = data.address;
    customer.login = login;
    this.logger.log('customer registered successfully');
    return this.registerRepository.save(customer);
  }

  //checking user is logined or not
  async loginUser(data: UserLogin, @Res() response: Response) {
    const user = await this.registerRepository.findOne({
      firstName: data.name
    });
    console.log(user);
    if (!user) {
      throw new BadRequestException('invalid credentials');
    }
    if (!(await bcrypt.compare(data.password, user.password))) {
      throw new BadRequestException('pasward incorrect');
    }
    const jwt = await this.jwtService.signAsync({ id: user.id });
    response.cookie('jwt', jwt, { httpOnly: true });
    return {
      message: 'success'
    };
  }

  // find user using cookie
  async findUser(request: Request) {
    try {
      const cookie = request.cookies['jwt'];

      const data = await this.jwtService.verifyAsync(cookie);
      if (!data) {
        throw new UnauthorizedException();
      }
      const user = await this.registerRepository.findOne({ id: data.id });
      const { password, ...result } = user;
      return result;
    } catch (e) {
      throw new UnauthorizedException();
    }
  }

  getCustomer(): Promise<Register[]> {
    return this.registerRepository
      .find()
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'no users in the list',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException('no users in the list', HttpStatus.NOT_FOUND);
      });
  }

  getCustomerByName(firstName: string) {
    return this.registerRepository
      .findOne({ firstName })
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'user name not found',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException('file not found ', HttpStatus.NOT_FOUND);
      });
  }

  updateCustomer(id, data: Register): string {
    const customer = new Register();
    const login = new UserLogin();
    customer.firstName = login.name = data.firstName;
    customer.password = login.password = data.password;
    customer.lastName = data.lastName;
    customer.email = data.email;
    customer.phoneNo = data.phoneNo;
    customer.address = data.address;
    customer.login = login;

    this.loginRepository.update({ id: id }, login);
    this.registerRepository.update({ id: id }, data);
    this.logger.log('customer updated successfully');
    return 'updated customer details successfully';
  }

  deleteCustomer(id: number) {
    this.logger.log('customer deleted successfully');
    return this.registerRepository.delete({ id });
  }

  //logout user
  async logout(@Res({ passthrough: true }) response: Response) {
    response.clearCookie('jwt');
    return {
      message: 'logout'
    };
  }
}
