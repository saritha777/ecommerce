import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { UserLogin } from './login.entity';

@Entity()
export class Register {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column({ unique: true })
  firstName: string;

  @ApiProperty()
  @IsString()
  @Column()
  lastName: string;

  @ApiProperty()
  @IsString()
  @Column()
  password: string;

  @ApiProperty()
  @IsString()
  @Column()
  email: String;

  @ApiProperty()
  @IsInt()
  @Column()
  phoneNo: number;

  @ApiProperty()
  @IsString()
  @Column()
  address: String;

  @OneToOne(() => UserLogin, (login) => login.register, {
    cascade: true
  })
  login: UserLogin;
}
