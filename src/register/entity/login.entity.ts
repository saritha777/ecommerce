import { Payment } from '../../payment/payment.entity';
import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { Register } from './register.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity()
export class UserLogin {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column()
  name: string;

  @ApiProperty()
  @Column()
  password: string;

  @OneToOne(() => Register, (user) => user.login, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
  })
  @JoinColumn()
  register: Register;

  @OneToOne(() => Payment, (user) => user.login, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
  })
  @JoinColumn()
  payment: Payment;
}
