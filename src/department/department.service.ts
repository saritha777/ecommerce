import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Departments } from './department.entity';

@Injectable()
export class DepartmentsService {
  logger: Logger;
  constructor(
    @InjectRepository(Departments)
    private departmentsRepository: Repository<Departments>
  ) {
    this.logger = new Logger(DepartmentsService.name);
  }
  async addDepartments(department: Departments) {
    const departmentsdata = new Departments();
    departmentsdata.electronics = department.electronics;
    departmentsdata.homeAppliances = department.homeAppliances;
    departmentsdata.createdTime = department.createdTime;
    departmentsdata.createdBy = department.createdBy;
    departmentsdata.updatedBy = department.updatedBy;
    departmentsdata.updatedTime = department.updatedTime;
    this.logger.log('department data added successfully');
    return await this.departmentsRepository.save(departmentsdata);
  }
  getAllDepartments(): Promise<Departments[]> {
    this.logger.log('getting all departments data');
    return this.departmentsRepository.find();
  }
  async getDepartmentsDataByUserId(id: number): Promise<Departments> {
    this.logger.log(`getting department by id ${id}`);
    return await this.departmentsRepository.findOne({ id });
  }
  updateDepartments(id: number, department: Departments) {
    const departmentsdata = new Departments();
    departmentsdata.electronics = department.electronics;
    departmentsdata.homeAppliances = department.homeAppliances;
    departmentsdata.createdTime = department.createdTime;
    departmentsdata.createdBy = department.createdBy;
    departmentsdata.updatedBy = department.updatedBy;
    departmentsdata.updatedTime = department.updatedTime;
    this.logger.log('departments data updated successfully');
    this.departmentsRepository.update({ id: id }, department);
    return 'successfully updated';
  }
  async deleteDepartments(id: number) {
    this.logger.log('department data deleted successfully');
    return await this.departmentsRepository.delete({ id });
  }
}
