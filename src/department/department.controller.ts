import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Departments } from './department.entity';
import { DepartmentsService } from './department.service';

@ApiTags('departments')
@Controller('departments')
export class DepartmentsController {
  constructor(private readonly departmentsService: DepartmentsService) {}
  @Post('/customer')
  addDepartments(@Body() departments: Departments): Promise<Departments> {
    return this.departmentsService.addDepartments(departments);
  }
  @Get('/customer')
  getAllDepartments(): Promise<Departments[]> {
    return this.departmentsService.getAllDepartments();
  }
  @Get('userId/:id')
  getDepartmentsDataByUserId(@Param('id') id: number): Promise<Departments> {
    return this.departmentsService.getDepartmentsDataByUserId(id);
  }
  @Put('/userId/:id')
  updateDepartments(@Param('id') id: number, @Body() departments: Departments) {
    return this.departmentsService.updateDepartments(id, departments);
  }
  @Delete('user/:id')
  deleteDepartments(@Param('id') id: number) {
    return this.departmentsService.deleteDepartments(id);
  }
}
