import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Watches } from './watch.entity';
import { WatchController } from './watches.controller';
import { WatchService } from './watches.service';

@Module({
  imports: [TypeOrmModule.forFeature([Watches])],
  controllers: [WatchController],
  providers: [WatchService]
})
export class WatchModule {}
