import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Delete
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Watches } from './watch.entity';
import { WatchService } from './watches.service';

@ApiTags('Electronics-Watch')
@Controller('/Watch')
export class WatchController {
  constructor(private readonly watchService: WatchService) {}

  @Post()
  addWatch(@Body() watch: Watches): Promise<Watches> {
    return this.watchService.addWatch(watch);
  }

  @Get()
  getWatch(): Promise<Watches[]> {
    return this.watchService.getWatch();
  }

  @Get('/brand/:brandName')
  getWatchByBrand(@Param('brandName') brandName: string): Promise<Watches> {
    return this.watchService.getWatchByBrand(brandName);
  }

  @Get('/type/:type')
  getWatchByType(@Param('type') type: string): Promise<Watches> {
    return this.watchService.getWatchByType(type);
  }

  @Put('/:id')
  updateWatch(@Param('id') id: number, @Body() watch: Watches): string {
    return this.watchService.updateWatch(id, watch);
  }

  @Delete('/:id')
  deleteWatch(@Param('id') id: number) {
    return this.watchService.deleteWatch(id);
  }
}
