import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { customException } from '../../../customexception/custom.exception';
import { Repository } from 'typeorm';
import { Watches } from './watch.entity';

@Injectable()
export class WatchService {
  logger: Logger;
  constructor(
    @InjectRepository(Watches)
    private watchRepository: Repository<Watches>
  ) {
    this.logger = new Logger(WatchService.name);
  }

  async addWatch(watch: Watches) {
    const watchdata = new Watches();
    watchdata.brandName = watch.brandName;
    watchdata.colour = watch.colour;
    watchdata.compatibleDevices = watch.compatibleDevices;
    watchdata.description = watch.description;
    watchdata.modelName = watch.modelName;
    watchdata.modelYear = watch.modelYear;
    watchdata.price = watch.price;
    watchdata.rating = watch.rating;
    watchdata.type = watch.type;
    watchdata.warranty = watch.warranty;

    this.logger.log('watch data added successfully');
    return await this.watchRepository.save(watchdata);
  }

  async getWatch(): Promise<Watches[]> {
    this.logger.log('getting all watch data');
    return await this.watchRepository
      .find()
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'watch not available in the store',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException(
          'watch not available in the store',
          HttpStatus.NOT_FOUND
        );
      });
  }

  async getWatchByBrand(brandName: string): Promise<Watches> {
    this.logger.log('getting watch data by id');
    return await this.watchRepository
      .findOne({ brandName })
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'watch not available for this brand',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException(
          'watch not available for this brand',
          HttpStatus.NOT_FOUND
        );
      });
  }

  async getWatchByType(type: string): Promise<Watches> {
    this.logger.log('getting watch data by type');
    return await this.watchRepository
      .findOne({ type })
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'watch not available for this type',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException(
          'watch not available for this type',
          HttpStatus.NOT_FOUND
        );
      });
  }

  updateWatch(id: number, watch: Watches) {
    const watchdata = new Watches();
    watchdata.brandName = watch.brandName;
    watchdata.colour = watch.colour;
    watchdata.compatibleDevices = watch.compatibleDevices;
    watchdata.description = watch.description;
    watchdata.modelName = watch.modelName;
    watchdata.modelYear = watch.modelYear;
    watchdata.price = watch.price;
    watchdata.rating = watch.rating;
    watchdata.type = watch.type;
    watchdata.warranty = watch.warranty;

    this.logger.log('watch data updated successfully');
    this.watchRepository.update({ id: id }, watch);
    return 'updated watch data successfully';
  }

  async deleteWatch(id: number) {
    this.logger.log('watch data deleted successfully');
    return await this.watchRepository.delete({ id });
  }
}
