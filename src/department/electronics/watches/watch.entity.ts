import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Specification } from '../electronics.specification';

@Entity()
export class Watches extends Specification {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column()
  colour: string;

  @ApiProperty()
  @IsString()
  @Column()
  compatibleDevices: string;

  @ApiProperty()
  @IsString()
  @Column()
  type: string;

  @ApiProperty()
  @IsInt()
  @Column()
  modelYear: number;
}
