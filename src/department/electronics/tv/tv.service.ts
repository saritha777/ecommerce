import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { customException } from '../../../customexception/custom.exception';
import { Repository } from 'typeorm';
import { Tv } from './television.entity';

@Injectable()
export class TvService {
  logger: Logger;
  constructor(
    @InjectRepository(Tv)
    private tvRepository: Repository<Tv>
  ) {
    this.logger = new Logger(TvService.name);
  }

  async addTv(tv: Tv) {
    const tvdata = new Tv();
    tvdata.brandName = tv.brandName;
    tvdata.description = tv.description;
    tvdata.displayTechnology = tv.displayTechnology;
    tvdata.modelName = tv.modelName;
    tvdata.modelYear = tv.modelYear;
    tvdata.price = tv.price;
    tvdata.rating = tv.rating;
    tvdata.resolution = tv.resolution;
    tvdata.screenSize = tv.screenSize;
    tvdata.warranty = tv.warranty;

    this.logger.log('tv data added successfully');
    return await this.tvRepository.save(tvdata);
  }

  async getTv(): Promise<Tv[]> {
    this.logger.log('getting all tv data');
    return await this.tvRepository
      .find()
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'no tvs in the store',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException('no tvs in the store', HttpStatus.NOT_FOUND);
      });
  }

  async getTvByBrand(brandName: string): Promise<Tv> {
    this.logger.log('getting tv data by brand');
    return await this.tvRepository
      .findOne({ brandName })
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'tv not available for this brand',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException(
          'tv not available for this brand',
          HttpStatus.NOT_FOUND
        );
      });
  }

  async getTvByInches(screenSize: number): Promise<Tv> {
    this.logger.log('getting tv data by screen size');
    return await this.tvRepository
      .findOne({ screenSize })
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'tv not available for this screen size',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException(
          'tv not available for this screen size',
          HttpStatus.NOT_FOUND
        );
      });
  }

  updateTv(id: number, tv: Tv) {
    const tvdata = new Tv();
    tvdata.brandName = tv.brandName;
    tvdata.description = tv.description;
    tvdata.displayTechnology = tv.displayTechnology;
    tvdata.modelName = tv.modelName;
    tvdata.modelYear = tv.modelYear;
    tvdata.price = tv.price;
    tvdata.rating = tv.rating;
    tvdata.resolution = tv.resolution;
    tvdata.screenSize = tv.screenSize;
    tvdata.warranty = tv.warranty;

    this.logger.log('tv data updated successfully');
    this.tvRepository.update({ id: id }, tv);
    return 'updated tv data successfully';
  }

  async deleteTv(id: number) {
    this.logger.log('tv data deleted successfully');
    return await this.tvRepository.delete({ id });
  }
}
