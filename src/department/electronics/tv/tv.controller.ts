import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Delete
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Tv } from './television.entity';
import { TvService } from './tv.service';

@ApiTags('Electronics-Tv')
@Controller('/Tv')
export class TvController {
  constructor(private readonly tvService: TvService) {}

  @Post()
  addTv(@Body() tv: Tv): Promise<Tv> {
    return this.tvService.addTv(tv);
  }

  @Get()
  getTv(): Promise<Tv[]> {
    return this.tvService.getTv();
  }

  @Get('/screen/:screenSize')
  getTvByInches(@Param('screenSize') screenSize: number): Promise<Tv> {
    return this.tvService.getTvByInches(screenSize);
  }

  @Get('/brand/:brandName')
  getTvByBrand(@Param('brandName') brandName: string): Promise<Tv> {
    return this.tvService.getTvByBrand(brandName);
  }

  @Put('/:id')
  updateTv(@Param('id') id: number, @Body() tv: Tv): string {
    return this.tvService.updateTv(id, tv);
  }

  @Delete('/:id')
  deleteTv(@Param('id') id: number) {
    return this.tvService.deleteTv(id);
  }
}
