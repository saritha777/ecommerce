import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString, isString } from 'class-validator';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Specification } from '../electronics.specification';

@Entity()
export class Tv extends Specification {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsInt()
  @Column()
  screenSize: number;

  @ApiProperty()
  @IsString()
  @Column()
  resolution: string;

  @ApiProperty()
  @IsString()
  @Column()
  displayTechnology: string;

  @ApiProperty()
  @IsInt()
  @Column()
  modelYear: number;
}
