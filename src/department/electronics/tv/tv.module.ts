import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Tv } from './television.entity';
import { TvController } from './tv.controller';
import { TvService } from './tv.service';

@Module({
  imports: [TypeOrmModule.forFeature([Tv])],
  controllers: [TvController],
  providers: [TvService]
})
export class TvModule {}
