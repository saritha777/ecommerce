import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import { Column, Entity } from 'typeorm';

@Entity()
export class Specification {
  @ApiProperty()
  @IsString()
  @Column()
  brandName: string;

  @ApiProperty()
  @IsString()
  @Column()
  rating: string;

  @ApiProperty()
  @IsString()
  @Column()
  description: string;

  @ApiProperty()
  @IsInt()
  @Column()
  price: number;

  @ApiProperty()
  @IsString()
  @Column()
  modelName: string;

  @ApiProperty()
  @IsInt()
  @Column()
  warranty: number;
}
