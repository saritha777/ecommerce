import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Specification } from '../electronics.specification';

@Entity()
export class Mobile extends Specification {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column()
  colour: string;

  @ApiProperty()
  @IsInt()
  @Column()
  memoryCapacity: number;

  @ApiProperty()
  @IsInt()
  @Column()
  camQuality: number;

  @ApiProperty()
  @IsInt()
  @Column()
  batteryCapacity: number;
}
