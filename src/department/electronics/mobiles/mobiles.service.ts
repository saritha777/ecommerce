import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { customException } from '../../../customexception/custom.exception';
import { Repository } from 'typeorm';
import { Mobile } from './mobile.entity';

@Injectable()
export class MobileService {
  logger: Logger;
  constructor(
    @InjectRepository(Mobile)
    private mobileRepository: Repository<Mobile>
  ) {
    this.logger = new Logger(MobileService.name);
  }

  async addMobile(mobile: Mobile) {
    const mobiledata = new Mobile();
    mobiledata.batteryCapacity = mobile.batteryCapacity;
    mobiledata.brandName = mobile.brandName;
    mobiledata.camQuality = mobile.camQuality;
    mobiledata.colour = mobile.colour;
    mobiledata.description = mobile.description;
    mobiledata.memoryCapacity = mobile.memoryCapacity;
    mobiledata.modelName = mobile.modelName;
    mobiledata.price = mobile.price;
    mobiledata.rating = mobile.rating;
    mobiledata.warranty = mobile.warranty;

    this.logger.log('mobile data added successfully');
    return await this.mobileRepository.save(mobiledata);
  }

  async getMobile(): Promise<Mobile[]> {
    this.logger.log('getting all mobiles data');
    return await this.mobileRepository
      .find()
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'no mobiles in the list',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException(
          'no mobiles in the list',
          HttpStatus.NOT_FOUND
        );
      });
  }

  async getMobileByBrand(brandName: string): Promise<Mobile> {
    this.logger.log('getting mobiles data by brand');
    return await this.mobileRepository
      .findOne({ brandName })
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'mobile not available for this brand',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException(
          'mobile not available for this brand',
          HttpStatus.NOT_FOUND
        );
      });
  }

  updateMobile(id: number, mobile: Mobile) {
    const mobiledata = new Mobile();
    mobiledata.batteryCapacity = mobile.batteryCapacity;
    mobiledata.brandName = mobile.brandName;
    mobiledata.camQuality = mobile.camQuality;
    mobiledata.colour = mobile.colour;
    mobiledata.description = mobile.description;
    mobiledata.memoryCapacity = mobile.memoryCapacity;
    mobiledata.modelName = mobile.modelName;
    mobiledata.price = mobile.price;
    mobiledata.rating = mobile.rating;
    mobiledata.warranty = mobile.warranty;

    this.logger.log('mobile data updated successfully');
    this.mobileRepository.update({ id: id }, mobile);
    return 'updated mobile data successfully';
  }

  async deleteMobile(id: number) {
    this.logger.log('mobile data deleted successfully');
    return await this.mobileRepository.delete({ id });
  }
}
