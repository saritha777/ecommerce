import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Mobile } from './mobile.entity';
import { MobileService } from './mobiles.service';

@ApiTags('Electronics-Mobiles')
@Controller('/Mobiles')
export class MobileController {
  constructor(private readonly mobileService: MobileService) {}

  @Post()
  addMobile(@Body() mobile: Mobile): Promise<Mobile> {
    return this.mobileService.addMobile(mobile);
  }

  @Get()
  getMobile(): Promise<Mobile[]> {
    return this.mobileService.getMobile();
  }

  @Get('/:brandName')
  getMobileByBrand(@Param('brandName') brandName: string): Promise<Mobile> {
    return this.mobileService.getMobileByBrand(brandName);
  }

  @Put('/:id')
  updateMobile(@Param('id') id: number, @Body() mobile: Mobile): string {
    return this.mobileService.updateMobile(id, mobile);
  }

  @Delete('/:id')
  deleteMobile(@Param('id') id: number) {
    return this.mobileService.deleteMobile(id);
  }
}
