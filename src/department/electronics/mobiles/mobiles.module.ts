import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Mobile } from './mobile.entity';
import { MobileController } from './mobiles.controller';
import { MobileService } from './mobiles.service';

@Module({
  imports: [TypeOrmModule.forFeature([Mobile])],
  controllers: [MobileController],
  providers: [MobileService]
})
export class MobileModule {}
