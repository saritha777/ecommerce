import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { HomeSpecifications } from '../homeSpecification';

@Entity()
export class WashingMachine extends HomeSpecifications {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column()
  configuration: string;

  @ApiProperty()
  @IsString()
  @Column()
  model: string;

  @ApiProperty()
  @IsInt()
  @Column()
  dimensions: number;

  @ApiProperty()
  @IsString()
  @Column()
  controlTypes: string;

  @ApiProperty()
  @IsInt()
  @Column()
  itemWeight: number;
}
