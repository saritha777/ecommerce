import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Delete
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { WashingMachine } from './WashingMachine.entity';
import { WashingMachineService } from './washingmachine.service';

@ApiTags('HomeAppliances-Washing Machine')
@Controller('/WashingMachine')
export class WashingMachineController {
  constructor(private readonly washingmachineService: WashingMachineService) {}

  @Post()
  addWashingMachine(
    @Body() washingmachine: WashingMachine
  ): Promise<WashingMachine> {
    return this.washingmachineService.addWashingMachine(washingmachine);
  }

  @Get()
  getWashingMachine(): Promise<WashingMachine[]> {
    return this.washingmachineService.getWashingMachine();
  }

  @Get('/brand/:brand')
  getWashingMachineByBrand(
    @Param('brand') brand: string
  ): Promise<WashingMachine> {
    return this.washingmachineService.getWashingMachineByBrand(brand);
  }

  @Get('/capacity/:capacity')
  getWashingMachineByCapacity(
    @Param('capacity') capacity: string
  ): Promise<WashingMachine> {
    return this.washingmachineService.getWashingMachineByCapacity(capacity);
  }

  @Put('/:id')
  updateWashingMachine(
    @Param('id') id: number,
    @Body() washingmachine: WashingMachine
  ): string {
    return this.washingmachineService.updateWashingMachine(id, washingmachine);
  }

  @Delete('/:id')
  deleteWashingMachine(@Param('id') id: number) {
    return this.washingmachineService.deleteWashingMachine(id);
  }
}
