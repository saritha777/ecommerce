import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { customException } from '../../../customexception/custom.exception';
import { Repository } from 'typeorm';
import { WashingMachine } from './WashingMachine.entity';
import { identity } from 'rxjs';

@Injectable()
export class WashingMachineService {
  logger: Logger;
  constructor(
    @InjectRepository(WashingMachine)
    private washingmachineRepository: Repository<WashingMachine>
  ) {
    this.logger = new Logger(WashingMachineService.name);
  }

  async addWashingMachine(washingmachine: WashingMachine) {
    const washingmachinedata = new WashingMachine();
    washingmachinedata.brand = washingmachine.brand;
    washingmachinedata.capacity = washingmachine.capacity;
    washingmachinedata.colour = washingmachine.colour;
    washingmachinedata.configuration = washingmachine.configuration;
    washingmachinedata.description = washingmachine.description;
    washingmachinedata.model = washingmachine.model;
    washingmachinedata.controlTypes = washingmachine.controlTypes;
    washingmachinedata.dimensions = washingmachine.dimensions;
    washingmachinedata.itemWeight = washingmachine.itemWeight;
    washingmachinedata.price = washingmachine.price;
    washingmachinedata.rating = washingmachine.rating;
    washingmachinedata.warranty = washingmachine.warranty;

    this.logger.log('washing machine data added successfully');
    return await this.washingmachineRepository.save(washingmachinedata);
  }

  async getWashingMachine(): Promise<WashingMachine[]> {
    this.logger.log('getting all washing machines data');
    return await this.washingmachineRepository
      .find()
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'Washing Machine not available in the store',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException(
          'Washing Machine not available in the store',
          HttpStatus.NOT_FOUND
        );
      });
  }

  async getWashingMachineByBrand(brand: string): Promise<WashingMachine> {
    this.logger.log('getting washing machine data by id');
    return await this.washingmachineRepository
      .findOne({ brand })
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'Washing Machine not available for this brand',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException(
          'Washing Machine not available for this brand',
          HttpStatus.NOT_FOUND
        );
      });
  }

  async getWashingMachineByCapacity(capacity: string): Promise<WashingMachine> {
    this.logger.log('getting washing machine data by capacity');
    return await this.washingmachineRepository
      .findOne({ capacity })
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'Washing Machine not available for this capacity',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException(
          'Washing Machine not available for this capacity',
          HttpStatus.NOT_FOUND
        );
      });
  }

  updateWashingMachine(id: number, washingmachine: WashingMachine) {
    const washingmachinedata = new WashingMachine();
    washingmachinedata.brand = washingmachine.brand;
    washingmachinedata.capacity = washingmachine.capacity;
    washingmachinedata.colour = washingmachine.colour;
    washingmachinedata.configuration = washingmachine.configuration;
    washingmachinedata.description = washingmachine.description;
    washingmachinedata.model = washingmachine.model;
    washingmachinedata.controlTypes = washingmachinedata.controlTypes;
    washingmachinedata.dimensions = washingmachinedata.dimensions;
    washingmachinedata.itemWeight = washingmachinedata.itemWeight;
    washingmachinedata.price = washingmachine.price;
    washingmachinedata.rating = washingmachine.rating;
    washingmachinedata.warranty = washingmachine.warranty;

    this.logger.log('washing machine data updated successfully');
    this.washingmachineRepository.update({ id: id }, washingmachine);
    return 'updated Washingmachine data successfully';
  }

  async deleteWashingMachine(id: number) {
    this.logger.log('washing machine data deleted successfully');
    return await this.washingmachineRepository.delete({ id });
  }
}
