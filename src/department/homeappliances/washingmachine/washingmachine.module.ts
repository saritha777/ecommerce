import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WashingMachineController } from './washingmachine.controller';
import { WashingMachine } from './WashingMachine.entity';
import { WashingMachineService } from './washingmachine.service';

@Module({
  imports: [TypeOrmModule.forFeature([WashingMachine])],
  controllers: [WashingMachineController],
  providers: [WashingMachineService]
})
export class WashingMachineModule {}
