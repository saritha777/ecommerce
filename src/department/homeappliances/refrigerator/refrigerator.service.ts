import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { customException } from '../../../customexception/custom.exception';
import { Repository } from 'typeorm';
import { Refrigerator } from './Refrigerator.entity';

@Injectable()
export class RefrigeratorService {
  logger: Logger;
  constructor(
    @InjectRepository(Refrigerator)
    private refrigeratorRepository: Repository<Refrigerator>
  ) {
    this.logger = new Logger(RefrigeratorService.name);
  }

  async addRefrigerator(refrigerator: Refrigerator) {
    const refrigeratordata = new Refrigerator();

    refrigeratordata.brand = refrigerator.brand;
    refrigeratordata.capacity = refrigerator.capacity;
    refrigeratordata.colour = refrigerator.colour;
    refrigeratordata.configuration = refrigerator.configuration;
    refrigeratordata.description = refrigerator.description;
    refrigeratordata.model = refrigerator.model;
    refrigeratordata.price = refrigerator.price;
    refrigeratordata.rating = refrigerator.rating;
    refrigeratordata.warranty = refrigerator.warranty;

    this.logger.log('refrigerator data added successfully');
    return await this.refrigeratorRepository.save(refrigeratordata);
  }

  async getRefrigerator(): Promise<Refrigerator[]> {
    this.logger.log('getting all refrigerators data ');
    return await this.refrigeratorRepository
      .find()
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'Refrigerator not available in the store',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException(
          'Refrigerator not available in the store',
          HttpStatus.NOT_FOUND
        );
      });
  }

  async getRefrigeratorByBrand(brand: string): Promise<Refrigerator> {
    this.logger.log('getting refrigerator data by id');
    return await this.refrigeratorRepository
      .findOne({ brand })
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'Refrigerator not available for this brand',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException(
          'Refrigerator not available for this brand',
          HttpStatus.NOT_FOUND
        );
      });
  }

  async getRefrigeratorByCapacity(capacity: string): Promise<Refrigerator> {
    this.logger.log('getting refrigerator data by capacity');
    return await this.refrigeratorRepository
      .findOne({ capacity })
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'Refrigerator not available for this capacity',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException(
          'Refrigerator not available for this capacity',
          HttpStatus.NOT_FOUND
        );
      });
  }

  updateRefrigerator(id: number, refrigerator: Refrigerator) {
    const refrigeratordata = new Refrigerator();
    refrigeratordata.brand = refrigerator.brand;
    refrigeratordata.capacity = refrigerator.capacity;
    refrigeratordata.colour = refrigerator.colour;
    refrigeratordata.configuration = refrigerator.configuration;
    refrigeratordata.description = refrigerator.description;
    refrigeratordata.model = refrigerator.model;
    refrigeratordata.price = refrigerator.price;
    refrigeratordata.rating = refrigerator.rating;
    refrigeratordata.warranty = refrigerator.warranty;

    this.logger.log('refrigerator data updated successfully');
    this.refrigeratorRepository.update({ id: id }, refrigerator);
    return 'updated Refrigerator data successfully';
  }

  async deleteRefrigerator(id: number) {
    this.logger.log('refrigerator data deleted successfully');
    return await this.refrigeratorRepository.delete({ id });
  }
}
