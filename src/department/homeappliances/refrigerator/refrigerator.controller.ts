import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Delete
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Refrigerator } from './Refrigerator.entity';
import { RefrigeratorService } from './refrigerator.service';

@ApiTags('HomeAppliances-Fridge')
@Controller('/Refrigerator')
export class RefrigeratorController {
  constructor(private readonly refrigeratorService: RefrigeratorService) {}

  @Post()
  addRefrigerator(@Body() refrigerator: Refrigerator): Promise<Refrigerator> {
    return this.refrigeratorService.addRefrigerator(refrigerator);
  }

  @Get()
  getRefrigerator(): Promise<Refrigerator[]> {
    return this.refrigeratorService.getRefrigerator();
  }

  @Get('/brand/:brand')
  getRefrigeratorByBrand(@Param('brand') brand: string): Promise<Refrigerator> {
    return this.refrigeratorService.getRefrigeratorByBrand(brand);
  }

  @Get('/capacity/:capacity')
  getRefrigeratorByCapacity(
    @Param('capacity') capacity: string
  ): Promise<Refrigerator> {
    return this.refrigeratorService.getRefrigeratorByCapacity(capacity);
  }

  @Put('/:id')
  updateRefrigerator(
    @Param('id') id: number,
    @Body() refrigerator: Refrigerator
  ): string {
    return this.refrigeratorService.updateRefrigerator(id, refrigerator);
  }

  @Delete('/:id')
  deleteRefrigerator(@Param('id') id: number) {
    return this.refrigeratorService.deleteRefrigerator(id);
  }
}
