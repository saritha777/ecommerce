import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RefrigeratorController } from './refrigerator.controller';
import { Refrigerator } from './Refrigerator.entity';
import { RefrigeratorService } from './refrigerator.service';

@Module({
  imports: [TypeOrmModule.forFeature([Refrigerator])],
  controllers: [RefrigeratorController],
  providers: [RefrigeratorService]
})
export class RefrigeratorModule {}
