import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { customException } from '../../../customexception/custom.exception';
import { Repository } from 'typeorm';
import { AirConditioner } from './ac.entity';

@Injectable()
export class ACService {
  logger: Logger;
  constructor(
    @InjectRepository(AirConditioner)
    private acRepository: Repository<AirConditioner>
  ) {
    this.logger = new Logger(ACService.name);
  }

  async addAc(ac: AirConditioner) {
    const acdata = new AirConditioner();
    acdata.brand = ac.brand;
    acdata.capacity = ac.capacity;
    acdata.colour = ac.colour;
    acdata.configuration = ac.configuration;
    acdata.description = ac.description;
    acdata.includedComponents = ac.includedComponents;
    acdata.model = ac.model;
    acdata.price = ac.price;
    acdata.rating = ac.rating;
    acdata.warranty = ac.warranty;
    this.logger.log('ac data added successfully');
    return await this.acRepository.save(acdata);
  }

  async getAc(): Promise<AirConditioner[]> {
    this.logger.log('getting all ac data');
    return await this.acRepository
      .find()
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'ac not available in the store',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException(
          'ac not available in the store',
          HttpStatus.NOT_FOUND
        );
      });
  }

  async getAcByBrand(brand: string): Promise<AirConditioner> {
    this.logger.log('getting ac by brand');
    return await this.acRepository
      .findOne({ brand })
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'ac not available for this brand',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException(
          'ac not available for this brand',
          HttpStatus.NOT_FOUND
        );
      });
  }

  async getAcByCapacity(capacity: string): Promise<AirConditioner> {
    this.logger.log('getting ac by capacity');
    return await this.acRepository
      .findOne({ capacity })
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'ac not available for this capacity',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException(
          'ac not available for this capacity',
          HttpStatus.NOT_FOUND
        );
      });
  }

  updateAc(id: number, ac: AirConditioner) {
    const acdata = new AirConditioner();
    acdata.brand = ac.brand;
    acdata.capacity = ac.capacity;
    acdata.colour = ac.colour;
    acdata.configuration = ac.configuration;
    acdata.description = ac.description;
    acdata.includedComponents = ac.includedComponents;
    acdata.model = ac.model;
    acdata.price = ac.price;
    acdata.rating = ac.rating;
    acdata.warranty = ac.warranty;

    this.logger.log('ac data updated successfully');
    this.acRepository.update({ id: id }, ac);
    return 'updated AC data successfully';
  }

  async deleteAc(id: number) {
    this.logger.log('ac data deleted successfully');
    return await this.acRepository.delete({ id });
  }
}
