import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ACController } from './AC.controller';
import { AirConditioner } from './AC.entity';
import { ACService } from './AC.service';

@Module({
  imports: [TypeOrmModule.forFeature([AirConditioner])],
  controllers: [ACController],
  providers: [ACService]
})
export class ACModule {}
