import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Delete
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AirConditioner } from './AC.entity';
import { ACService } from './AC.service';

@ApiTags('HomeAppliances-AC')
@Controller('/AC')
export class ACController {
  constructor(private readonly acService: ACService) {}

  @Post()
  addAc(@Body() ac: AirConditioner): Promise<AirConditioner> {
    return this.acService.addAc(ac);
  }

  @Get()
  getAc(): Promise<AirConditioner[]> {
    return this.acService.getAc();
  }

  @Get('/brand/:brand')
  getAcByBrand(@Param('brand') brand: string): Promise<AirConditioner> {
    return this.acService.getAcByBrand(brand);
  }

  @Get('/capacity/:capacity')
  getAcByCapacity(
    @Param('capacity') capacity: string
  ): Promise<AirConditioner> {
    return this.acService.getAcByCapacity(capacity);
  }

  @Put('/:id')
  updateAc(@Param('id') id: number, @Body() ac: AirConditioner): string {
    return this.acService.updateAc(id, ac);
  }

  @Delete('/:id')
  deleteAc(@Param('id') id: number) {
    return this.acService.deleteAc(id);
  }
}
