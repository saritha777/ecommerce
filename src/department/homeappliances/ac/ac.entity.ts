import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { HomeSpecifications } from '../homeSpecification';

@Entity()
export class AirConditioner extends HomeSpecifications {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column()
  configuration: string;

  @ApiProperty()
  @IsString()
  @Column()
  model: string;

  @ApiProperty()
  @IsString()
  @Column()
  includedComponents: string;
}
