import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity } from 'typeorm';

@Entity()
export class HomeSpecifications {
  @ApiProperty()
  @Column()
  brand: string;

  @ApiProperty()
  @Column()
  colour: string;

  @ApiProperty()
  @Column()
  capacity: string;

  @ApiProperty()
  @Column()
  price: number;

  @ApiProperty()
  @Column()
  description: string;

  @ApiProperty()
  @Column()
  rating: number;

  @ApiProperty()
  @Column()
  warranty: number;
}
