import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Departments {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column()
  electronics: string;

  @ApiProperty()
  @IsString()
  @Column()
  homeAppliances: string;

  @ApiProperty()
  @IsString()
  @Column()
  createdTime: string;

  @ApiProperty()
  @IsString()
  @Column()
  updatedBy: string;

  @ApiProperty()
  @IsString()
  @Column()
  createdBy: string;

  @ApiProperty()
  @IsString()
  @Column()
  updatedTime: string;
}
