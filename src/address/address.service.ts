import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { customException } from '../customexception/custom.exception';
import { Repository } from 'typeorm';
import { Address } from './address.entity';

@Injectable()
export class AddressService {
  logger: Logger;
  constructor(
    @InjectRepository(Address)
    private addressRepository: Repository<Address>
  ) {
    this.logger = new Logger(AddressService.name);
  }

  addAddress(data: Address): Promise<Address> {
    const address = new Address();
    address.city = data.city;
    address.country = data.country;
    address.houseNo = data.houseNo;
    address.pincode = data.pincode;
    address.state = data.state;
    address.streetName = data.streetName;
    address.streetNo = data.streetNo;

    this.logger.log('address added successfully');
    return this.addressRepository.save(address);
  }

  getAddress(): Promise<Address[]> {
    this.logger.log('getting all address');
    return this.addressRepository
      .find()
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'no address details in the list please add it..',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException(
          'no address details in the list please add it..',
          HttpStatus.NOT_FOUND
        );
      });
  }

  getAddressById(id: number): Promise<Address> {
    return this.addressRepository
      .findOne({ addressId: id })
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'no address available for the id in the list',
            HttpStatus.BAD_REQUEST
          );
        }
      })
      .catch(() => {
        throw new customException(
          'no address available for the id in the list',
          HttpStatus.BAD_REQUEST
        );
      });
  }

  updateAddress(id, data: Address): string {
    const address = new Address();
    address.city = data.city;
    address.country = data.country;
    address.houseNo = data.houseNo;
    address.pincode = data.pincode;
    address.state = data.state;
    address.streetName = data.streetName;
    address.streetNo = data.streetNo;

    this.logger.log('address updated successfully');
    this.addressRepository.update({ addressId: id }, data);
    return 'updated customer details successfully';
  }

  deleteAddress(id: number) {
    this.logger.log('address deleted successfully');
    this.addressRepository.delete({ addressId: id });
  }
}
