import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Address {
  @PrimaryGeneratedColumn()
  addressId: number;

  @ApiProperty()
  @IsString()
  @IsInt()
  @Column()
  streetNo: number;

  @ApiProperty()
  @IsString()
  @Column()
  streetName: string;

  @ApiProperty()
  @IsInt()
  @Column()
  houseNo: number;

  @ApiProperty()
  @IsString()
  @Column()
  city: string;

  @ApiProperty()
  @IsString()
  @Column()
  state: string;

  @ApiProperty()
  @IsString()
  @Column()
  country: string;

  @ApiProperty()
  @IsInt()
  @Column()
  pincode: number;
}
