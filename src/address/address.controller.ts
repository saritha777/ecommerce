import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Address } from './address.entity';
import { AddressService } from './address.service';

@ApiTags('Address')
@Controller('/address')
export class AddressController {
  constructor(private readonly addressService: AddressService) {}

  @Post()
  addAddress(@Body() data: Address): Promise<Address> {
    return this.addressService.addAddress(data);
  }

  @Get()
  getAddress(): Promise<Address[]> {
    return this.addressService.getAddress();
  }

  @Get('/:id')
  getAddressById(@Param('id') id: number): Promise<Address> {
    return this.addressService.getAddressById(id);
  }

  @Put('/:id')
  updateCustomer(@Param('id') id: number, @Body() data: Address): string {
    return this.addressService.updateAddress(id, data);
  }

  @Delete('/:id')
  deleteCustomer(@Param('id') id: number) {
    return this.addressService.deleteAddress(id);
  }
}
